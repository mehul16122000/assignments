import "./App.css";
import Login from "./components/Login";
import NewPage from "./components/NewPage";
import { Redirect } from "react-router-dom";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import NewUser from "./components/NewUser";
function App() {
  return (
    <>
      <Router>
        <Switch>
          <Route path="/login">
            <Login />
          </Route>
          <Route path="/newpage">
            <NewPage authorized={true} />
          </Route>
          <Route>
            <Redirect to="/login" />
          </Route>
        </Switch>
      </Router>
      {/* <NewUser /> */}
    </>
  );
}

export default App;
