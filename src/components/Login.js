import React, { useState } from "react";
import users from "../json/users.json";
import "./Login.css";
import CryptoJS from "react";
import { AES, enc } from "crypto-js";
import NewPage from "./NewPage";
import { useHistory } from "react-router-dom";

const getPassword = () => {
  let password = localStorage.getItem("password");
  console.log(password);
  if (password) {
    return JSON.stringify(localStorage.getItem("password"));
  } else {
    return "";
  }
};
const Login = () => {
  const [data, setData] = useState({
    fname: "",
    lname: "",
    email: "",
    password: "",
  });
  let history = useHistory();

  const Enryption = () => {
    const envryptedString = AES.encrypt(data.password, "MM16EHUL");
    localStorage.setItem("encryptedmsg", envryptedString.toString());
    const decrypted = AES.decrypt(
      localStorage.getItem("encryptedmsg"),
      "MM16EHUL"
    );
    const decryptedString = decrypted.toString(enc.Utf8);

    console.log(decryptedString);
  };
  const HandleInput = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    setData({ ...data, [name]: value });
  };
  const signUp = (e) => {
    e.preventDefault();

    for (let d of users.users) {
      if (d.email === data.email && d.password === data.password) {
        Enryption();
        history.push("/newpage");
      }
    }
    // return alert("user is not authenticate");
  };

  return (
    <>
      <div className="container my-5">
        <h2>Login page</h2>
        <label className="label-email">Email</label>
        <br></br>
        <input
          className="input-email"
          placeholder="Enter your Email"
          type="text"
          name="email"
          value={data.email}
          onChange={HandleInput}
        ></input>
        <br></br>
        <label className="label-password">Password</label>
        <br></br>
        <input
          className="input-password"
          placeholder="Enter your password"
          type="password"
          name="password"
          value={data.password}
          onChange={HandleInput}
        ></input>
        <br></br>
        <br></br>
        <button className="btn-primary" onClick={signUp}>
          Login
        </button>
      </div>
    </>
  );
};
export default Login;
