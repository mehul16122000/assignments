import * as React from "react";
import { AES, enc } from "crypto-js";
import users from "../json/users.json";
import array from "../json/invitations.json";

const NewUser = (props) => {
  const decrypted = AES.decrypt(
    localStorage.getItem("encryptedmsg"),
    "MM16EHUL"
  );
  const decryptedString = decrypted.toString(enc.Utf8);
  //   console.log(decryptedString);
  const localData = users.users.filter((e) => {
    return e.password === decryptedString;
  });
  //   console.log(localData);
  //   console.log(props);
  const style = {
    Active: {
      color: "green",
    },
    InActive: {
      color: "read",
    },
  };
  return (
    <>
      {props.data
        .filter((check, key) => {
          return check.sender_id === localData[0].first_name;
        })
        .map((e) => {
          return (
            <ul key={e.invite_id}>
              <li>{e.sender_id}</li>
              <li>{e.sig_id}</li>
              <li>{e.vector}</li>
              <li style={e.status === "read" ? style.Active : style.I}>
                {e.status}
              </li>
            </ul>
          );
        })}
    </>
  );
};
export default NewUser;
