import React, { useState, useEffect } from "react";
import { Redirect } from "react-router-dom";
import { useHistory } from "react-router-dom";
import invite from "../json/invitations.json";
import users from "../json/users.json";
import NewUser from "./NewUser";
import { AES, enc } from "crypto-js";
import newInvites from "../json/invitations_update.json";
import "./NewPage.css";

const NewPage = ({ authorized }) => {
  const [newList, setNewList] = useState(invite.invites);
  const decryptedItem = () => {
    const decrypted = AES.decrypt(
      localStorage.getItem("encryptedmsg"),
      "MM16EHUL"
    );
    const decryptedString = decrypted.toString(enc.Utf8);
    //   console.log(decryptedString);
    const localData = users.users.filter((e) => {
      return e.password === decryptedString;
    });

    return localData[0].first_name;
  };

  const localUserData = decryptedItem();

  const [updateList, setUpdateList] = useState(
    newInvites.invites.filter((e) => {
      return e.sender_id === localUserData;
    })
  );

  useEffect(() => {
    if (updateList.length !== 0) {
      const interval = setInterval(() => {
        if (updateList.length) {
          let newData = updateList;
          let data = newData.shift();
          setNewList([...newList, data]);
          setUpdateList(newData);
        }
        return clearInterval(interval);
      }, 5000);
    }
  }, [newList, updateList]);

  let history = useHistory();
  if (!authorized) {
    return <Redirect to="/login" />;
  }

  return (
    <>
      <nav className="navbar mx-2 my-2">
        <div className="container-fluid">
          <a className="navbar-brand" href="/">
            Navbar
          </a>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav">
              <li className="nav-item"></li>
            </ul>
          </div>
        </div>
      </nav>
      <button className="btn-button" onClick={() => history.push("/login")}>
        Logout
      </button>
      <NewUser data={newList}></NewUser>
    </>
  );
};
export default NewPage;
